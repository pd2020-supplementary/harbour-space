### Important links
* [video lectures](https://drive.google.com/drive/folders/1SkcX17RwNVY-upmSbQlEj-huoCs_PcrB?usp=sharing)
* [presentations](https://drive.google.com/drive/folders/1YrPXokESY_CETcb_YYkIeKVMq1CWN7J3?usp=sharing)
* [the table with results](https://docs.google.com/spreadsheets/d/1yp1iQne4DvfCUetmSR-NP9vw9OcpbCglx9-RAUZVS-Y/edit#gid=0)
* The code examples are located at `/home/velkerr/seminars/pd2020/` on the Hadoop cluster.
