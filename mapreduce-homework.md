# MapReduce homework
In this home assignment, 2 tasks need to be solved. The solution must be done in Java using the Hadoop Java API (http://hadoop.apache.org/docs/r2.6.1/api/) or using Hadoop Streaming. When solving problems, try to use the optimal MapReduce algorithm:
* use as little Hadoop Job as possible,
* use more than 1 reducer (1 reducer is allowed to be used only in the final job, when sorting the result)

Deadline: 22.01.2020, 23:59

# Input datasets
#### List of identifiers (for the task 1):
* The path to the entire dataset in HDFS: `/data/ids`
* The path to the sample dataset in HDFS: `/data/ids_part`
* File format: text
* Line format: single identifier per line

#### Wikipedia:
* The path the entire dataset in HDFS: `/data/wiki/en_articles`
* The path to the sample dataset in HDFS: `/data/wiki/en_articles_part`
* File format: text
* Line format: `article ID <tab> article text`

Use the path to the entire dataset for submitting and use the sample (small dataset) for debugging.

# Task 1
Imagine, you have a Web service and you would like to complete the performance testing. You have real logs from your service. In each line you have a user ID (random string) who accessed Web service. You usually have 5 users using your service each second. To complete the performance testing, you need to generate the list of user IDs (in random order) requesting Web services at each second. Occasionally, you removed all the timestamps from logs. Therefore you have to randomly choose from 1 to 5 users for each second.
* Input: one identifier per line
* HDFS output format: id1,id2,...
* STDOUT: print the first 50 generated lines

Output example:
```
1cf54b530128257d72,4cdf3efa01036a9a48,8c3e7fb30261aaf9cf
4cfe6230016553c3ed,76e1b8690176f801bb,e7409c39013c9db7b4,a5f1519c02b22550e6
83a119ef02346d0879
```

# Task 2
|Name|Variant|
|--|--|
|Arjun	Nair|1|
|Riva	Fan|2|
|Leonardo	Fancjini|2|
|Jovan	Velanac|1|
|Nishit	Shah|2|

## Variant 1
You are asked to calculate the number of occurrences of each Named Entity. We call Named Entity each string from 6 to 9 characters in length, starting with a capital letter, all other subsequent letters have to be in lowercase. The last, but not the least property of the Named Entity is the following. Named Entity should never occur in the dataset with the first letter in lowercase. Extra request (for standardization purposes): transform each named entity into lowercase.
* Input: Wikipedia.
* HDFS output format: name <tab> number
* STDOUT: print TOP 10 named entities

##### Notes:
* make sure to remove all punctuation marks while parsing Wikipedia articles.
* make sure all the named entities are printed in lowercase
* sort (named entity, counts) pairs in the descending order on the number of occurrences (by value)
* If there are pairs with the same “counts” - sort them by keys (= by strings in a lexicographical order)

##### Output example:
```
english 8358
states 7264
british 6829
```

## Variant 2
We consider the words to be equal if they consist from the same letters (abc = bca). Your task is to find the most popular permutations. For each class of permutations, please also calculate the number of occurrences for 5 top-most permutations. Consider similar example from task 4 - “abc 100 abc:50;bca:50”
* Input: Wikipedia.
* HDFS output format: permutation_word <tab> number_of_occurences <tab>  word1:popularity1;...
* STDOUT: print TOP 10 permutations

##### Notes:
* remove all the punctuation marks
* remove all words shorter than 3 characters
* lowercase all the words
* sort (permutation_word, occurrences, counts) in the descending order on the number of occurrences
* if there are triples with the same “occurrences” (the same popularity) - sort them by permutation_word in a lexicographical order.

##### Output example:
```
asw 91833 was:90214;saw:1604;asw:11;aws:2;wsa:1;
for 89924 for:89912;fro:6;rof:4;rfo:1;ofr:1;
ahtt 81085 that:81085;
```

# How to submit the task
1. You have the own repository in `http://gitlab.atp-fivt.org` named `YOUR_LOGIN-mapreduce` and 2 branches (for tasks 1 and 2 respectively) created in it.
2. In each branch you should create a directory, directory name = branch name. For example, you need to create directories named `mapreducetask1` and` mapreducetask2` in the branches `mapreducetask1` and` mapreducetask2`, respectively.
3. Put the `run.sh` file in the created directories. This is the entry point into your program and it will be launched by the verification system. In `run.sh` there can be either the whole solution of the problem or a call to other files.

> Even if you develop in Python, `run.sh` can only contain a call to the desired script, for example:
>
> #! /usr/bin/env bash
>
> python my_python_script.py $ *
>
> Here $ * will provide the parameters with which `run.sh` was called to` my_python_script.py`.
4. If the automatically testing is Ok, create merge request from the branch to master.

[Detailed information](https://docs.google.com/presentation/d/1eDxnTeBWSB1OrA3BwEUa2vJAJm3_OJMuoRxyA13RzTY/edit#slide=id.p) about BigJudge grading system (in russian).

You also have the repository named `YOUR_LOGIN-demo`. In this repo you can practice with the grading system. You can submit [this code](http://gitlab.atp-fivt.org/root/demos/tree/master) in `YOUR_LOGIN-demo`, demotask1 branch.

# General Guidelines
Please, pay attention to the following guidelines. If you don’t obey the guidelines, then the grading system may incorrectly mark your solution as failed.
1. For temporary data use user HDFS folder with random suffix (time or any random number)
2. Make sure to remove temp folders after completing the assignment
3. Keep track of the status of MapReduce jobs. In case of failure of the first job, you do not need to run the subsequent ones.
4. Please refer to the "STDOUT" requirement of your task. The format should be the same as the HDFS output format. You only need to read the required number of lines from HDFS output and print them to STDOUT.
5. You can sort the portion of the data on-the-fly. For instance, if you already have HDFS output, then you can head 50 lines and sort them with the help of shell.
6. You CANNOT read the entire HDFS output (file) into RAM and sort it.

Usually you can use absolute and relative paths. We ask you to use relative paths for HDFS intermediate and output folders. It is the requirement from our grading system - we do not have permissions to write into your home folder. Therefore we update paths on the fly. If you have absolute paths, it will not work.
* Absolute path example: `/user/hsp202001/some_folder`
* Relative path example: `some_folder`
